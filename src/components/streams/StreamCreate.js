import React, { Component } from 'react';
import { connect } from "react-redux";
import { createStream } from "../../actions";
import StreamForm from "./StreamForm";

export class StreamCreate extends Component {

    onSubmit = (formValues) => {
        this.props.createStream(formValues);
    }
    render() {
        return (
            <div>
                <h3>Create Stream</h3>
                <StreamForm onSubmit={this.onSubmit} key="new" />
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return { }
}

const mapDispatchToProps = {
    createStream: createStream
}


export default connect(mapStateToProps, mapDispatchToProps)(StreamCreate);