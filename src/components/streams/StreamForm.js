import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';

export class StreamForm extends Component {
    renderError(meta) {
        const { error, touched } = meta;
        if (touched && error) {
            return (
                <div>{error}</div>
            )
        }
    }
    renderInput = (formProps) => {
        const { input, label, type, meta } = formProps;
        return (
            <div className="form-group">
                <label>{label}</label>
                <input type={type} {...input} className="form-control" />
                {this.renderError(meta)}
                {/* <div>{meta.error}</div> */}
            </div>
        );
    }
    onSubmit = (formValues) => {
        this.props.onSubmit(formValues);
    }
    render() {
        return (
            <div>
                <form onSubmit={this.props.handleSubmit(this.onSubmit)}>
                    <Field name="title" component={this.renderInput} label="Enter title" type="text" />
                    <Field name="description" component={this.renderInput} label="Enter description" type="text" />
                    <button className="btn btn-primary">Submit</button>
                </form>
            </div>
        )
    }
}

const validate = (formValues) => {
    const errors = {};
    if (!formValues.title) {
        errors.title = "You must enter a title";
    }
    if (!formValues.description) {
        errors.description = "You must enter a description";
    }
    return errors;
}


export default reduxForm({
    form: "StreamForm",
    validate: validate
})(StreamForm)
