import React, { Component } from 'react';
import { connect } from 'react-redux';
import flv from "flv.js";
import { fetchStream } from "../../actions";

export class StreamShow extends Component {
    constructor() {
        super();
        this.videoRef = React.createRef();
    }

    componentDidMount() {
        const { id } = this.props.match.params;
        this.props.fetchStream(id);
        this.buildPlayer();
    }
    componentDidUpdate(){
        this.buildPlayer();
    }
    componentWillUnmount(){
        this.player.destroy();
    }
    buildPlayer() {
        if (this.player || !this.props.stream) {
            return null;
        }
        const { id } = this.props.match.params;
        this.player = flv.createPlayer({
            type: "flv",
            url: `http://localhost:8000/live/${id}.flv`
        });
        this.player.attachMediaElement(this.videoRef.current);
        this.player.load();
    }
    render() {
        if (!this.props.stream) {
            return <div><h1>Loading...</h1></div>
        }
        const { title, description } = this.props.stream;
        return (
            <div>
                <video ref={this.videoRef} style={{ width: "100%" }} controls />
                <h3>{title}</h3>
                <p>{description}</p>
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    return { stream: state.streams[props.match.params.id] }
}

const mapDispatchToProps = {
    fetchStream: fetchStream
}

export default connect(mapStateToProps, mapDispatchToProps)(StreamShow)
