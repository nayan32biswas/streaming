import _ from "lodash";
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchStream, editStream } from "../../actions";
import StreamForm from "./StreamForm";

export class StreamEdit extends Component {
    componentDidMount(){
        this.props.fetchStream(this.props.match.params.id);
    }
    renderStream(){
        return (
            <div>
                <h4>{this.props.stream.title}</h4>
                <p>{this.props.stream.description}</p>
            </div>
        );
    }
    onSubmit = (formValues) =>{
        this.props.editStream(this.props.match.params.id, formValues);
    }
    render() {
        if(!this.props.stream){
            return (
                <div>
                    <h1>Loading...</h1>
                </div>
            )
        }
        console.log(this.props.stream)
        return (
            <div>
                <h3>Edit Stream</h3>
                <StreamForm initialValues={_.pick(this.props.stream, "title", "description")} onSubmit={this.onSubmit}/>
                {/* {this.renderStream()} */}
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    return {stream: state.streams[props.match.params.id]}
}

const mapDispatchToProps = {
    fetchStream: fetchStream,
    editStream: editStream 
}

export default connect(mapStateToProps, mapDispatchToProps)(StreamEdit)
