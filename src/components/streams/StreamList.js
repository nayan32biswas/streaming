import React, { Component } from 'react';
import {Link} from "react-router-dom";
import { connect } from 'react-redux';
import { fetchStreams } from "../../actions";

export class StreamList extends Component {
    componentDidMount() {
        this.props.fetchStreams();
    }
    renderAdmin(stream) {
        if (stream.userId === this.props.userId) {
            return (
                <div >
                    <Link to={`/stream/edit/${stream.id}`} className="btn btn-primary mr-1">edit</Link>
                    <Link to={`/stream/delete/${stream.id}`} className="btn btn-danger">delete</Link>
                </div>
            );
        }
        return null;
    }
    renderList() {
        return this.props.streams.map(stream => {
            return (
                <div key={stream.id} className="card flex-row flex-wrap">
                    <div className="card-block ml-1">
                        <h5 className="modal-title"><Link to={`stream/${stream.id}`}>{stream.title}</Link></h5>
                        <p >{stream.description}</p>
                    </div>
                    <div className="card-header ml-auto">
                        {this.renderAdmin(stream)}
                    </div>
                </div>
            );
        })
    }
    renderCreateStream() {
        if (this.props.isSignedIn) {
            return (
                <div>
                    <Link to="stream/new" className="btn btn-primary ">Create New Stream</Link>
                </div>
            );
        }
        return null;
    }
    render() {
        return (
            <div>
                {this.renderList()}
                {this.renderCreateStream()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        streams: Object.values(state.streams),
        userId: state.auth.userId,
        isSignedIn: state.auth.isSignedIn
    };
}

const mapDispatchToProps = {
    fetchStreams: fetchStreams
}

export default connect(mapStateToProps, mapDispatchToProps)(StreamList)
