import React, { Component } from 'react';
import ReactDOM from "react-dom";

export default class Modal extends Component {
    render() {
        return ReactDOM.createPortal(
            <>
                <div onClick={this.props.onDismiss} className="modal fade show" style={{ display: "block", backgroundColor: "gray" }}>
                    <div className="modal-dialog" role="document" onClick={(event) => event.stopPropagation()}>
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">{this.props.title}</h5>
                                <button onClick={this.props.onDismiss} type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">{this.props.content}</div>
                            <div className="modal-footer">
                                {this.props.actions}
                            </div>
                        </div>
                    </div>
                </div>
            </>
            ,
            document.querySelector("#self_modal")
        )
    }
}
