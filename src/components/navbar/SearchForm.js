import React, { Component } from 'react';
import "./style.css";
export default class SearchForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            query: ""
        }
    }
    onSubmitHandler = (event) => {
        event.preventDefault();
        this.props.onFormSubmit(this.state);
    }
    onChangeHandler = (event) => {
        var data = event.target.value;
        this.setState({ query: data });
    }
    render() {
        return (
            <div className="">
                <form className="searchForm-form form-inline" onSubmit={this.onSubmitHandler}>
                    <div className="form-group">
                        <input onChange={this.onChangeHandler} type="text" className="SearchForm--form form-control mr-sm-2" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Search for product" />
                        {/* <button onClick={this.onSubmitHandler} className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> */}
                    </div>
                </form>
            </div>
        );
    }
}
