import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thank from "redux-thunk";

import App from './components/App';
import reducers from './reducers';

import { compose } from 'redux';
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// Redux thank with React DevTools
const store = createStore(reducers, composeEnhancers(applyMiddleware(thank))); 
// Redux thank
// const store = createStore(reducers, applyMiddleware(thank)); 

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);